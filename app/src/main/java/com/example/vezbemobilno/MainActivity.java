package com.example.vezbemobilno;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_app1:
                Intent intent = new Intent(this, Kalkulator.class);
                startActivity(intent);
                break;
            case R.id.btn_app2: Toast.makeText(MainActivity.this, "APP2", Toast.LENGTH_LONG).show(); break;
            case R.id.btn_app3: Toast.makeText(MainActivity.this, "APP3", Toast.LENGTH_LONG).show(); break;
            case R.id.btn_app4: Toast.makeText(MainActivity.this, "APP4", Toast.LENGTH_LONG).show(); break;
        }
    }

}
