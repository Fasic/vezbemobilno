package com.example.vezbemobilno;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {
    final String USERNAME = "";
    final String PASSWORD = "";


    EditText username, password;
    Button login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = (EditText) findViewById(R.id.username_et);
        password = (EditText) findViewById(R.id.password_et);
        login = (Button) findViewById(R.id.login_bt);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(username.getText().toString().equals(USERNAME) && password.getText().toString().equals(PASSWORD)){
                    Intent intent = new Intent(Login.this, MainActivity.class);
                    intent.putExtra("USERNAME", username.getText().toString());
                    startActivity(intent);
                }
                else{
                    Toast.makeText(Login.this, "Username ili sifra nisu dobri!", Toast.LENGTH_LONG).show();
                }
            }
        });

    }
}
